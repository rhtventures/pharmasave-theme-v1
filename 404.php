<!DOCTYPE html>
<html <?php language_attributes(); ?> class="h-100">

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">

  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" />
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/pharmasavefox.css" rel="stylesheet" />
 
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Roboto+Slab:300,400,700" rel="stylesheet">

  <?php wp_head(); ?>

  <!-- JS -->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/viewpointchecker.js"></script>
    
</head>
<body class="page-error text-center m-0 p-0">
  
    <div class="bg-dark text-white mb-5 p-2">
      <h2> <?php echo get_option( 'pharmacy_store_name_full' );?></h2>
    </div>
  
    <div class="container h-100">
       <div class="row pt-5">

          <div class="col-md-12 pt-3 pb-2">

            <a href="/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/404.png" alt="Page Doesn't Exist" title="404" class="mb-3 img-fluid"/> </a> 

          <div class="error-content">

            <h1> PAGE NOT FOUND </h1>

            <h3> The page you are lookng for might have been removed, had its name changed or is temporarily unavailable.</h3>
            <br/>
            <a href="/" class="home-page text-white"> HOME PAGE</a>
          </div> 

        </div> 
      </div><!--/.row-->
    </div><!--/container-->
 </body>

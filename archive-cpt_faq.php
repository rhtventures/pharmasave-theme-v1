<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>

          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
               
              <?php              
                echo '<div class="row border-bottom pb-4 mb-2">';
                    echo '<div class="col-12">';
                        echo '<div class="faq-q">';
                            echo ''.get_field('faq_question').'';
                        echo '</div>';
                        
                        echo '<div class="bg-light p-3">';
                              echo ''.get_field('faq_answer').'';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
              ?>
             
          <?php endwhile; endif; ?>
        
         <div class="row">
          <div class="col-12">
 
              <?php else : ?>
                There are currently no news items.
              <?php endif; ?>
          </div>
           <div class="col-12 pt-2">

            <?php echo ''.the_posts_pagination();?>
         </div>
      </div>
        
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
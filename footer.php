<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>
	
<footer class="pb-4">
  <div class="footer-menu">
    
       <div class="container">

          <div class="row justify-content-md-center pt-3 mb-4">
              <?php wp_nav_menu(
                  array(
                    'depth'             => 1,//sub menu - increase to 2
                    'container'         => '',
                    'container_id'      => '',
                    'container_class'   => '',
                    'menu_class' 		=> 'list-unstyled',
                    'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
                    'menu_id'			=> 'main-menu',
                  )
                ); ?> 
        </div>
    </div>
  </div>
  
  <div class="container">
    <div class="row justify-content-md-center locations pb-4 mb-3">
      
        <?php          
         $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
         while ( $loop->have_posts() ) : $loop->the_post();
        ?>
        <div class="col-md-3 text-center location">
            <h6 class="font-weight-bold"><?php the_field('location_name'); ?></h6>
            <?php the_field('location_address_street'); ?>
            <?php the_field('location_address_city'); ?><br/>
          <?php the_field('location_address_city'); ?>, <?php the_field('location_address_province'); ?><br/>
            <strong>P:</strong> <a href="tel:<?php the_field('location_phone_number'); ?>" class="white"><?php the_field('location_phone_number'); ?></a> 
          
            <?php if( get_field('location_fax_number') ): ?>
              | <strong>F:</strong> <a href="tel:<?php the_field('location_fax_number'); ?>" class="white"><?php the_field('location_fax_number'); ?></a>
           <?php endif; ?>
              
             <p><a href="<?php echo get_permalink(); ?>#hours">Hours <i class="fas fa-clock"></i></a> | <a href="<?php echo get_permalink(); ?>#map">Map <i class="fas fa-map-marker"></i></a></p>
        </div>
        <?php endwhile; ?>
     
    </div>
    <div class="d-block text-center">
    
			  <p><i class="fa fa-comment"></i> CONNECT WITH US </p>
			
        <div class="social my-3">
           
            <?php if( get_option('pharmacy_facebook_url') ): ?>
                <a href="<?php echo get_option('pharmacy_facebook_url') ?>" target="_blank"><i class="fab fa-facebook"></i></a>
            <?php endif; ?>

            <?php if( get_option('pharmacy_twitter_url') ): ?>
                <a href="<?php echo get_option('pharmacy_twitter_url') ?>" target="_blank"><i class="fab fa-twitter"></i></a>
            <?php endif; ?>

            <?php if( get_option('pharmacy_instagram_url') ): ?>
                <a href="<?php echo get_option('pharmacy_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i></a>
            <?php endif; ?>

            <?php if( get_option('pharmacy_linkedin_url') ): ?>
               <a href=" <?php echo get_option('pharmacy_linkedin_url') ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            <?php endif; ?>

            <?php if( get_option('pharmacy_youtube_url') ): ?>
                <a href="<?php echo get_option('pharmacy_youtube_url') ?>" target="_blank"><i class="fab fa-youtube"></i></a>
            <?php endif; ?>
         
        </div>
			
		  <p class="copy">&copy; <?php echo date("Y");?>  Pharmasave.  All rights reserved. <a href="/privacy-policy">Privacy Policy</a></p>
    
    </div>
	</div><!--/.container-->
</footer>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/global.js"></script>

 <?php wp_footer(); ?>

</body>

</html>
<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-lg-9 pb-2">
        
        <div class="row">
 
            <?php              
              echo '<div class="col-sm-5">';
                  echo '<div class="border p-4">';
                      $image = get_field('product_image');
                      echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
                  echo '</div>';
              echo '</div>';
                
              echo '<div class="col-sm-7">';
                  echo '<h1>'.get_the_title().'</h1>';
                  echo '<span class="font20"><strong><sup>$</sup>'.get_field('product_price').'</strong></span><br/><br/>';
                  echo '<strong>Product Description:</strong><br/> '.get_field('product_description').'';
                      
                  echo '<p><a href="/products"><i class="fas fa-arrow-left"></i> View more Products</a>';
              
               echo '</div>';
             ?>

        </div>
         
      </div>
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-3">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>

          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
                              

              <?php              
                echo '<div class="border-bottom pb-3 mb-3">';
                    echo ''.get_field('testimonial_quote').'';

                    echo '<p><strong>'.get_field('testimonial_fname'). '&nbsp;' .get_field('testimonial_lname'). '</strong> | ';
                    echo '<strong>'.get_field('testimonial_title').'</strong></p>';
                echo '</div>';
 
              ?>
              
          <?php endwhile; endif; ?>
        
          
        
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
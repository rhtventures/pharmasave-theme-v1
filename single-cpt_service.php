<?php get_header(); ?>

<div class="page-space mt-5 mb-3">&nbsp;</div>

<section>
    <div class="container ">
        <div class="text-center bg-red p-5">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
           <h1 class="white"><?php echo get_the_title(); ?></h1>
         </div>
      
        <?php get_template_part('/page-templates-parts/sub-nav'); ?>
         
    </div><!--/.container-->
</section>
<section class="py-5 page-content">
    <div class="container"> 
         
          <?php      
        
            echo '<div class="row mb-4">';
                  echo '<div class="col-12 pt-4">';

      
                          echo '<div class="services-icon">';
                            $icon = get_field('service_icon');
                            echo '<img class="mb-3" src="'.$icon['url'].'" alt="'.$icon['alt'].'" title="'.$icon['title'].'"/>';
                          echo '</div>';
      
                         echo '<div class="py-3 pt-md-5 border-top border-bottom">';
                          echo ''.get_field('service_description').'';
                        echo '</div>';
                          
                     
                  echo '</div>';

            echo '</div>';//row
      
            echo '<div class="row mb-4 border-bottom">';
                echo '<div class="col-12 text-center">';
                    echo '<p><a  class="btn btn-small text-white" href="/services"><i class="fas fa-arrow-left"></i> View more Services</a>';
               echo '</div>';
                  echo '</div>';
           ?>                  
 
      <?php endwhile;?>

      <?php else : ?>
        There are currently no services.
      <?php endif; ?> 
    
     </div><!--/.container-->
</section>
 
<section>
 
    <div class="container">
        <div class="row pb-5 mb-5 border-bottom">
             <?php get_template_part('/page-templates-parts/ad-row'); ?>            
        </div><!--/.row-->
    </div><!--/.container-->
</section> 

<?php 	get_footer(); ?>
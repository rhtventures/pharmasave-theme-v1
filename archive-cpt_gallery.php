<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
     
     
      <div class="col-lg-9 pb-2">
        
          <h1><?php post_type_archive_title(); ?></h1>
        
          <div class="row">
         
          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
           
               <?php              
                
                  echo '<div class="col-6 col-md-4 mb-3">';
            
                      echo '<div class="image-crop">';
                          $image = get_field('gallery_image');
                          echo '<a href="'.get_permalink().'"><img class="ir" src="'.$image['url'].'" alt="'.$image['caption'].'" title="'.$image['title'].'"/></a>';
                       echo '</div>';

                    echo '<div class="text-center"><a href="'.get_permalink().'">'.get_the_title().'</a></div>';

                  echo '</div>';
                ?>
          
          <?php endwhile; endif; ?>
            
         <?php echo ''.the_posts_pagination();?>
        
      </div> </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>


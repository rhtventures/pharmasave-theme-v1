<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-1">
      
       <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>

          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
           
              <?php      
                echo '<div class="row border-bottom pb-4 mb-4">';
                  echo '<div class="col-md-5">';

                          echo '<h3>'.get_field('location_name').'</h3>';
        
                          $image = get_field('location_image');
                          echo '<img class="ir mb-1" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                          echo '<p><strong>Address:</strong><br/>';
                          echo ''.get_field('location_address_street').'<br/>';
                          echo ''.get_field('location_address_city').',&nbsp;';
                          echo ''.get_field('location_address_province').'&nbsp;';
                          echo ''.get_field('location_address_postal').'<br/>';
                          echo '<strong>Phone: </strong>'.get_field('location_phone_number').'&nbsp; | &nbsp;';
                          echo '<strong>Fax: </strong>'.get_field('location_fax_number').'</p>';
                          echo '<strong>Store Hours:</strong>';
                          echo ''.get_field('location_hours').'';
                          echo '<a href="'.get_permalink().'">Store Details</a> | <a href="/contact">Contact Store</a>';

                  echo '</div>';

                  echo '<div class="col-md-7 pt-5">';
        
                        echo '<div class="embed-responsive embed-responsive-16by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
         
         
                               
                  echo '</div>';
                echo '</div>';//row
        
               ?>
        
          <?php endwhile; endif; ?>
        
      </div><!--/left-column-->

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>


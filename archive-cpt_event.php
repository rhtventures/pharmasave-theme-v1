<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>

        <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  

          <?php    
              $date_start = str_replace('/', '-', get_field('event_date_start') );
              $date_end = str_replace('/', '-', get_field('event_date_end') );        
        
              echo '<div class="row border-bottom pb-4 mb-4">';
                  echo '<div class="col-sm-4">';

                      $image = get_field('event_image');
                      echo '<a href="'.get_permalink().'"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';

                  echo '</div>';

              echo '<div class="col-sm-8">' ;  

                  echo '<h3 class="mt-0 mb-1">'.get_the_title().'</h3>';

                  echo '<div class="date">';

                    if( $date_end != '' && $date_start != $date_end )
                        echo date( 'M d, Y', strtotime( $date_start ) ).'&nbsp;-&nbsp; '.date( 'M d, Y', strtotime( $date_end ) );
                    else
                        echo date( 'M d, Y', strtotime( $date_start ) );

                  echo '&nbsp;|&nbsp; '.get_field('event_location').'</div>';

                  echo wp_trim_words(get_field('event_description'), 60, '... <a href="'.get_permalink().'"> read more</a>' );

              echo '</div>';

            echo '</div>';

         ?>
 
        <?php endwhile;?>
        
        <?php else : ?>
          There are currently no events.
        <?php endif; ?>
        

        <?php echo ''.the_posts_pagination();?> 

       
          
          
        
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>


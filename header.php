<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">

  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" />
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/pharmasavefox.css" rel="stylesheet" />
  
  <!-- Fonts -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Roboto+Slab:300,400,700" rel="stylesheet">

  <?php wp_head(); ?>

  <!-- JS -->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/global.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
   
  
</head>

<body <?php body_class(); ?>>

  <div class="wrapper hfeed site" id="page">

    <header>
      <div class="container text-center relative pt-5 pb-4">

        <div id="text-adjust">
          <span>TEXT SIZE</span>
          <a class="ts-sm" onclick="javascript:decreaseFontSize();">-</a>
          <a class="ts-lg" onclick="javascript:increaseFontSize();">+</a>
        </div>
        <!--/#text-adjust-->

        <a href="/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="Pharmasave" title="Pharmasave" class="mb-3"/> </a>

        <h1>
          <?php echo get_option( 'pharmacy_store_name_full' );?>
        </h1>
        
        <?php if( get_option('pharmacy_tag_line') ): ?>
	           <span class="text-uppercase red font-weight-bold"><?php echo get_option( 'pharmacy_tag_line' );?></span><br/>
        <?php endif; ?>
       
        
       <div class="row sm-gutter justify-content-md-center pt-3 pb-2">
             <?php          
                $loop = new WP_Query( array('post_type' => 'cpt_location','orderby'=>'menu_order','order'=>'ASC','posts_per_page' => 100) );             
                while ( $loop->have_posts() ) : $loop->the_post();
            ?>
            <div class="col-md-3 text-center location">
                <?php if ( $loop->post_count > 1 ) echo '<strong>'.get_field('location_name').'</strong><br/>'; ?>
                <?php the_field('location_address_street'); ?> <br/>
                <?php the_field('location_address_city'); ?>, <?php the_field('location_address_province'); ?><br/>

                <strong>P:</strong> <a href="tel:<?php the_field('location_phone_number'); ?>" class="no-style"><?php the_field('location_phone_number'); ?></a>

                <?php if( get_field('location_fax_number') ): ?>
                | <strong>F:</strong> <a href="tel:<?php the_field('location_fax_number'); ?>" class="no-style"><?php the_field('location_fax_number'); ?></a>
                <?php endif; ?>

                <p> <a class="red" href="<?php echo get_permalink(); ?>#hours">Hours <i class="fas fa-clock"></i></a> | <a class="red" href="<?php echo get_permalink(); ?>#map">Map <i class="fas fa-map-marker"></i></a> </p>
            </div>
            <?php endwhile; ?>
         </div>
        

      </div>
    </header>
    <!--/#header-->
    <nav id="main-menu-nav">
        <div class="container relative">

            <?php wp_nav_menu(
            array(
              'depth'             => 2,//sub menu - increase to 2
              'container'         => '',
              'container_id'      => '',
              'container_class'   => '',
              'menu_class' 		=> 'list-unstyled',
              'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
              'menu_id'			=> 'main-menu',
            )
          ); ?>

          <div class="share">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url( $wp->request ); ?>" target="_blank">
             <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/share-facebook.png" border="0" alt="Share on Facebook"/> </a>

            <a href="https://twitter.com/home?status=<?php echo home_url( $wp->request ); ?>" target="_blank">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/share-twitter.png" border="0" alt="Share Twitter"/></a>
          </div>
          <!--/#share-->
       </div>
      
    </nav>
    
    <!-- Mobile Menu -->
    <div class="m-nav">
        <a href="#" class="reveal red"><i class="fas fa-bars"></i></a>
    </div>
    <div class="panel">
        <div class="panel-content">
            <div class="panel-close"></div>
               <?php wp_nav_menu(
                array(
                  'depth'             => 2,//sub menu - increase to 2
                  'container'         => '',
                  'container_id'      => '',
                  'container_class'   => '',
                  'menu_class' 		=> 'list-unstyled panel-content mb-0',
                  'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
                  'menu_id'			=> 'main-menu',
                )
              ); ?>

         </div>
     </div>
     <!-- Mobile Menu -->
    
    <script>
      $(window).scroll(function() {
        if ($(this).scrollTop() > 320) {
          $('#main-menu-nav').addClass("sticky");
        } else {
          $('#main-menu-nav').removeClass("sticky");
        }
      });
    </script>
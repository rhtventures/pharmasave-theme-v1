 // Font Size Increase/Decrease
 var min=8;
var max=18;

// ---------- Mobile Menu ---------- //

// Reveal & Close Panels
var revealPanel = function (buttonReveal, panel, buttonClose) {
  $(document).ready(function() {
    // Reveal panel 
    $(buttonReveal).on('click', function() {
      $(panel).addClass('expanded');
    });
    
    // Close panel
    $(buttonClose).on('click', function() {
      $(panel).removeClass('expanded');
    });   
    
    
  }); 
}

revealPanel('.reveal','.panel', '.panel-close');
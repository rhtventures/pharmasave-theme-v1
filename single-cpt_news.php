<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 ">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-lg-9 pb-2">
        
          <?php      

              echo '<div class="row mb-4">';
                    echo '<div class="col-sm-4">';

                       $image = get_field('news_image');
                       echo '<img class="ir" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                    echo '</div>';

                    echo '<div class="col-sm-8">';

                        echo '<h1 class="mb-1">'.get_the_title().'</h1>';
                        echo '<div class="date mb-2">'.the_date('F j, Y').'</div>';
                        echo ''.get_field('news_article').'';
                        echo '<p><a href="/news"><i class="fas fa-arrow-left"></i> View more News</a></p>';

                    echo '</div>';

              echo '</div>';//row

             ?>  
        
 
      </div>
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>



<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-3">
       
     
      <div class="col-lg-9 pb-2">
          <h1>Our Team</h1>

          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
       
            <?php      
              echo '<div class="row border-bottom pb-4 mb-4">';
                  echo '<div class="col-sm-3">';

                      $image = get_field('staff_image');
                      echo '<a href="'.get_permalink().'"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';

                  echo '</div>';

                  echo '<div class="col-sm-9">';

                      echo '<h4>'.get_field('staff_fname'). '&nbsp;' .get_field('staff_lname'). '</h4>';
                      echo '<h5>'.get_field('staff_title').'</h5>';  
                      echo wp_trim_words(get_field('staff_description'), 50, '... <a href="'.get_permalink().'"> read more</a>' );

                  echo '</div>';
               echo '</div>';//row

            ?>
           <?php endwhile; endif; ?>
        
          <?php echo ''.the_posts_pagination();?>
        
           
       </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
<?php /* Template Name: Email Signup */ ?>
<?php get_header(); ?>
 
<section>
	<div class="container py-5">
      <h1><?php single_post_title(); ?></h1>
    
      <div class="banner-image">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-email-signup.jpg" class="img-fluid mb-2"/>
      </div>
    
      <?php 
      if (have_posts()) {
        while (have_posts()) {
          the_post();
          the_content(); 
        }
      } ?>    
    
    <div class="row">
        <div class="col-12">
           <p>To receive deals, valuable health information & more through email from Pharmasave, please choose your location. </p>
        </div>
    </div>
    
    <div class="row pt-3">
      <?php          
       $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
       while ( $loop->have_posts() ) : $loop->the_post();
      ?>
      
      <div class="col-sm-6 col-lg-3 mb-3 text-center rx-refill-location">
           <a class="red" target="_blank" href="http://preferences.pharmasave.com/Preference.aspx?storeid=<?php the_field('location_store_id'); ?>">
           <div class="border py-5">
              <i class="fas fa-prescription-bottle-alt"></i>

              <h4 class="mb-2 text-dark"> <?php echo get_field('location_name') ?></h4>
              Get Your Prescription
           </div>
          </a>
      </div>
    
      <?php endwhile; ?>
  </div>
  </div>
</section>

<?php 	get_footer(); ?>
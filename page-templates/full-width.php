<?php /* Template Name: Full Width */ ?>
<?php get_header(); ?>
 
<section>
	<div class="container py-5">
      <h1><?php single_post_title(); ?></h1>

<?php 
if (have_posts()) {
  while (have_posts()) {
    the_post();
    the_content(); 
  }
} ?>
    
  </div>
</section>

<?php 	get_footer(); ?>
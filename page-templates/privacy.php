<?php /* Template Name: Privacy Policy */ ?>
<?php get_header(); ?>
 
<section>
	<div class="container">
    <p>&nbsp;</p>
      <h1><?php single_post_title(); ?></h1>

      <p>This Privacy Policy sets out the privacy policies and practices for <?php echo get_option( 'pharmacy_store_name_full' );?> (the “<strong>Pharmacy</strong>”) with respect to the collection, use, and disclosure of personal information in connection with 
<?php echo site_url(); ?> (the “<strong>Website</strong>”). This Privacy Policy may be updated by the Pharmacy giving reasonable notice of the revised terms (including by e-mail or by posting on the Website), and this Privacy Policy may be supplemented or modified by agreements entered into between the Pharmacy and an individual from time to time. In this Privacy Policy, “personal information” means any information about an identifiable individual, as further defined under applicable Canadian laws.</p>

      <h3><strong>Collection and Use of Personal Information</strong></h3>
      <p>The Pharmacy may collect personal information from a user of the Website (the “<strong>Site User</strong>”) when the Site User uses the Pharmacy website. The Pharmacy may use such personal information as is reasonably required to process the Site User’s registration for the Website and/or to provide information and other features on the website that may be useful to the Site User.</p> 

      <p>Contact information that the Site User provides to the Website may be used to contact him or her in connection with the Website. For instance, depending on which services the Site User has subscribed to and which privacy preferences the Site User has indicated to the Pharmacy, the Pharmacy may use the Site User’s contact information to provide notices regarding changes to the Website or regarding new or updates to the services or products.</p> 

      <p>The Pharmacy may collect and analyze non-personal information to evaluate how visitors use the Website, including non‑personal information about usage and service operation that is not associated with a specific personal identity. The Pharmacy also automatically receives and records information on the Pharmacy’s server logs, which information may include the Site User’s computer’s IP address and “cookie” information. The Pharmacy uses the Site User’s IP address to help identify the Site User, gather broad demographic information about the Pharmacy’s users, diagnose problems with the Pharmacy’s systems, and administer the Website. A cookie is a small text file that is stored on the Site User’s computer and cannot read data off of the Site User’s computer. The only information a cookie can collect is information supplied by the Site User.</p> 

      <p>The Pharmacy collects demographic and profile data in connection with the Website and uses such data to tailor each Site User's experience at the Website and displaying the content according to the Site User’s preferences.</p> 

      <p>The Pharmacy may use customer information for additional purposes that may be identified at or before the time that the information is collected. The Pharmacy will hold the personal information only as long as necessary for the purposes outlined in this Privacy Policy or as required or permitted by law.</p> 

      <p>The Pharmacy may share personal information with third-party subcontractors and agents who work on the Pharmacy’s behalf and third party service providers to who provide advice and services in relation to the website or in relation to products or services featured on the website. Such third parties do not have the right to use personal information beyond what is necessary to assist the Pharmacy and they are contractually obliged to maintain confidentiality and security of the personal information and are restricted from using such information in any way not expressly authorized by the Pharmacy.</p> 

      <h3><strong>Disclosure of Personal Information</strong></h3>
      <p>The Pharmacy may disclose personal information as follows:</p> 
      <ul>
      <li>to third party service providers and to affiliated entities in order to carry out work on behalf of the Pharmacy in relation to the website,</li>
      <li>to law enforcement agencies for the purposes of investigating fraud or other offences,</li>
      <li>to legal, financial, insurance, and other advisors or in connection with the sale, reorganization, or management of all or part of its business or operations, and</li>
      <li>as consented to by the Site User from time to time, including to fulfill any other purposes that are identified when the personal information is collected.</li>
      </ul>

      <p>The Pharmacy will not use or disclose personal information for purposes other than those for which it was collected, except with the Site User’s consent or as required or permitted by law. Consent may be express or implied, and given in writing by using or not using a check-off box, electronically, orally, or by the Site User’s conduct, such as accessing the Website and/or use of the services or products provided by the Website.</p> 

      <h3><strong>Storage of Personal Information</strong></h3>
      <p>Personal information collected by the Website is stored on servers in the United States. The personal information may also be managed by a third party processor on the Pharmacy’s behalf. While the law in the United States is less stringent than the laws applicable to Canada, the Pharmacy will process and transmit the Site User’s personal information in accordance with this Privacy Policy and applicable data protection or privacy legislation.</p> 

      <p>While in another jurisdiction for processing, the information may be accessed by the courts, law enforcement, and national security authorities.</p> 

      <h3><strong>Cookies and IP Addresses</strong></h3>
      <p>The Website is entitled to use cookies to deliver content according to the Site User’s preferences and to save the Site User’s password so that the Site User is not required to re-enter it while the Site User uses the Website.</p> 

      <h3><strong>Links</strong></h3>
      <p>The Website may contain links to other websites. The Pharmacy is not responsible for the privacy practices or the content of such websites. The Site User should review the privacy policies of the websites that he or she visits. This Privacy Policy applies solely to information collected by the Pharmacy with respect to the Website.</p> 

      <h3><strong>Security</strong></h3>
      <p>The Pharmacy protects against the loss, misuse, and alteration of personal information with security measures appropriate to the sensitivity of the information, including through the use of physical, organizational, and technological measures and appropriate training of employees. However, it is possible that any information transmitted via the Internet may be intercepted by unknown third parties. The Pharmacy is not responsible for the transmission of personal information that is disclosed or becomes transmitted through no omission or act of the Pharmacy.</p> 

      <h3><strong>Opt-Out</strong></h3>
      <p> When appropriate, Site Users are given the opportunity to (i) opt-out of receiving communications from the Pharmacy, (ii) remove their personal information from the Pharmacy’s records, or (iii) elect to no longer receive services from the Pharmacy. If the Site User wishes to opt-out of receiving further communications, please contact the Pharmacy’s by email or phone using the contact information provided below.</p> 

      <h3><strong>Corrections/Contact</strong></h3>
      <p>The Site User may contact the Pharmacy to modify or correct any of his or her personal information that is under the Pharmacy’s control. The Site User may also direct a written complaint regarding compliance with this Privacy Policy to the Pharmacy and, within a reasonable time upon receiving the written complaint, the Pharmacy will ask the Pharmasave regional office to conduct an investigation into the matter. Within a reasonable time of concluding the investigation, the Pharmasave regional office will inform the Site User whether the complaint will be allowed or denied. If the Pharmasave regional office allows the complaint, the Pharmacy will take appropriate measure necessary to rectify the source of the complaint.</p> 

      <h3><strong>Contact</strong></h3>
      <p>If the Site User has any questions about this Privacy Policy, the Pharmacy’s privacy practices in connection with this Website, or the Pharmacy’s collection, use, disclosure, or retention of the Site User’s personal information in connection with this Website, including with respect to use of service providers in the United States, please contact the Pharmacy as follows:</p> 
    
      Attention: Store Manager<br/><br/>
        <?php          
         $loop = new WP_Query( array('post_type' => 'cpt_location','meta_key'=> 'location_priority','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 100) );             
         while ( $loop->have_posts() ) : $loop->the_post();
        ?>        
            <strong><?php the_field('location_name'); ?></strong><br/>
            <?php the_field('location_address_street'); ?>, <?php the_field('location_address_city'); ?><br/>
            <?php the_field('location_phone_number'); ?><br/>
            <br/>            
        <?php endwhile; ?>        
        
        Email: <a href="mailto:<?php echo get_option( 'pharmacy_email' );?>"><?php echo get_option( 'pharmacy_email' );?></a></p>
   </div>
</section>

<?php 	get_footer(); ?>
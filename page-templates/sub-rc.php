<?php /* Template Name: Sub - Right column */ ?>
<?php get_header(); ?>
 
	
<section>
	<div class="container pt-4">
		<div class="row pb-5">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php single_post_title(); ?></h1>

          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
          <?php endwhile; endif; ?>
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('page-templates-parts/right-column'); ?>
        <?php echo the_field( 'content_1' ); ?>
         
        
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads sm-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
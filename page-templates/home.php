<?php /* Template Name: Home Page */ ?>
<?php get_header();?>

<div class="container">
		<section class="banner">

        <div id="homepage-banner" class="carousel slide carousel-fade" data-ride="carousel">
        <?php 
         $args = array(  
                  'post_type' => 'cpt_banner', 
                  'posts_per_page' => 100,
                  'orderby'	=> 'menu_order',
                  'order'	=> 'ASC',
                  'tax_query' => array(
                    array(
                        'taxonomy' => 'banner_locations',
                        'field'    => 'slug',
                        'terms'    => 'home-page-top'
                    ),
                   )
         );

        $loop = new WP_Query($args); 

        ?>   
       
          <ol class="carousel-indicators">
          <?php 
            $slide_count = 0;
            while ( $loop->have_posts() ) : $loop->the_post();          
                echo '<li data-target="#homepage-banner" data-slide-to="'.$slide_count.'" class="'.(( $slide_count == 0 )? 'active' : '' ).'"></li>';
                $slide_count++;
            endwhile; 
          ?>
          </ol>  
          
          <div class="carousel-inner">
          
       <?php 
          $slide_count = 0;
          while ( $loop->have_posts() ) : $loop->the_post();

              $image = get_field( 'banner_image' );  
              $link = get_field( 'banner_link' );

              echo '<div class="carousel-item '.(( $slide_count == 0 )? 'active' : '').'">';

              if( isset( $link['url'] ) )
                  echo '<a target="'.$link['target'].'" href="'.$link['url'].'" >';

              echo '<img src="'.$image['url'].'" class="img-fluid mb-3" alt="" title="'.get_the_title().'"/>';

              if( isset( $link['url'] ) )
                     echo '</a>';

              echo '</div>';
            
          $slide_count++;
          endwhile;
      ?>
				</div>
				
			</div>
 		</section>
		
		<section class="intro text-center py-4 myb-3">
      
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; endif; ?>

		</section><!--/.intro-->      
      
    <?php 
    //get post info for first location
    $loop = new WP_Query( array('post_type' => 'cpt_event','meta_key'=> 'event_date_start','orderby'=>'meta_value','order'=>'ASC','posts_per_page' => 1, 'meta_query'=> 
              array(
                array(
                  'key' => 'event_date_start',
                  'value' => date('Y-m-d'),
                  'compare' => '>=',
                  'type' => 'DATE'
                  )
                ) 
            ));   
    ?>
  
    <?php if( $loop->have_posts() ): ?>
  
    <section class="home-events text-center py-4 mb-5 border-bottom border-top"> 
          <div class="container">
            
              <h3 class="red text-center mb-5">Upcoming Events</h3>
              <div class="row justify-content-center ">

              <?php

                while ( $loop->have_posts() ) : $loop->the_post();


                $date_start = str_replace('/', '-', get_field('event_date_start') );
                $date_end = str_replace('/', '-', get_field('event_date_end') );                    
                
                  echo '<div class="col-lg-6">';
                
                      $image = get_field('event_image');
                       echo '<div class="image-crop text-left">';
                        echo '<a href="'.get_permalink().'"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                      echo '</div>';
                
                  echo '</div>';

                  echo '<div class="col-lg-6 text-left bg-light">';
                  echo '<div class="py-4 px-2">';

                      echo '<div class="relative">';

                         if( $date_end != '' && $date_start != $date_end )
                            echo date( 'M d, Y', strtotime( $date_start ) ).'&nbsp;-&nbsp; '.date( 'M d, Y', strtotime( $date_end ) );
                         else
                            echo date( 'M d, Y', strtotime( $date_start ) );
                
                          echo '</div>';


                          echo '<div class="pt-2">';
                              echo '<a href="'.get_permalink().'" class="no-style"><h4 class="mb-1 text-dark">'.get_the_title().'</h4></a>';
                              echo wp_trim_words(get_field('event_description'), 45, '...<a class="red" href="'.get_permalink().'" class="no-style"> Read More</a>' );
                               
                          echo '</div>';
                
                          echo '<div class="home-event-link">';
                              echo '<a  class="btn btn-small text-white" href="/events"> View more Events <i class="fas fa-arrow-right"></i></a>';
                          echo '</div>';
                      echo '</div>';
                      echo '</div>';

                   endwhile;
                ?>
            </div><!--/.row-->
         </div><!--/.container-->
      </section><!--/.events-->
  
    <?php endif;?>
  
		<section class="ads mb-5">
			<div class="row">
				
				<div class="col-md-12">
 					<div class="row ad-sm">
              <?php 

               $args = array(  
                        'post_type' => 'cpt_banner', 
                        'posts_per_page' => 100, 
                        'orderby'			=> 'menu_order',
                        'order'				=> 'ASC',
                        'tax_query' => array(
                          array(
                              'taxonomy' => 'banner_locations',
                              'field'    => 'slug',
                              'terms'    => 'home-page-bottom'
                          ),
                         )
               );
            
              $loop = new WP_Query($args);
            
              while ( $loop->have_posts() ) : $loop->the_post();
            
                $image = get_field( 'banner_image' );  
                $link = get_field( 'banner_link' );
                
                echo '<div class="col-md-'.(get_field( 'banner_blocks' )*3).'">';
            
                    if( isset( $link['url'] ) )
                        echo '<a target="'.$link['target'].'" href="'.$link['url'].'" >';

                        echo '<img width="100%" src="'.$image['url'].'" class="img-fluid mb-3" alt="'.get_the_title().'" title="'.get_the_title().'"/>';

                         if( isset( $link['url'] ) )
                         echo '</a>';
            
                 echo  '</div>';
      
              endwhile;
              ?>

 					</div>
 				</div>
				
			</div>
		</section>
			
        <?php 

         $args = array(  
                  'post_type' => 'cpt_product', 
                  'posts_per_page' => 100, 
                  'orderby'			=> 'menu_order',
                  'order'				=> 'ASC',
                  'meta_query' => array(
                         array(
                             'key' => 'featured',
                             'value' => true,
                             'compare' => '='
                         )                 
                   )
           );

          $loop = new WP_Query($args);
                
      if( $loop->have_posts() ):
      ?>
      <section class="mt-0 mb-5">
          <div class="row">
              <div class="col-12 border-top"><h3 class="text-center my-3">Featured Products</h3></div>    
      <?php

          while ( $loop->have_posts() ) : $loop->the_post();

            $image = get_field( 'product_image' );  

                echo '<div class="col-sm-4 col-md-2">';

                    echo '<div class="border p-2 pb-3 mb-3 mb-5 product">';

                        echo '<div class="image-crop">';
                           $image = get_field('product_image');
                           echo '<a href="'.get_permalink().'"><img class="ir" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                        echo '</div>';

                        echo '<div class="text-center">';
                            echo '<h5>'.get_the_title().'</h5>';
                             echo '<h3><sup>$</sup>'.get_field('product_price').'</h3>';
                        echo '</div>';

                   echo '</div>'; 
                echo '</div>'; 

          endwhile;
        ?> 
            
            <div class="col-12 text-center"> <a href="/products"> View All Products</a></div>    
		    <div> 
		</section>
    <?php endif; ?>
            
	</div><!--/.container-->

<?php 	get_footer(); ?>

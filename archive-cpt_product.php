<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>
        
          <div class="row products">
          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
       
              <?php              

                echo '<div class="col-6 col-sm-4 col-md-3">';
  
                    echo '<div class="border p-2 pb-3 mb-3 product">';
            
                        echo '<div class="image-crop">';
                           $image = get_field('product_image');
                           echo '<a href="'.get_permalink().'"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
                        echo '</div>';
  
                        echo '<div class="text-center">';
                            echo '<h4>'.get_the_title().'</h4>';
                             echo '<h2><sup>$</sup>'.get_field('product_price').'</h2>';
                        echo '</div>';
 
                   echo '</div>'; 
                echo '</div>';
              ?>
 
          <?php endwhile; endif; ?>
         </div>
        
         <div class="row">
          <div class="col-12">
 
              <?php else : ?>
                There are currently no news items.
              <?php endif; ?>
          </div>
           <div class="col-12 pt-2">

            <?php echo ''.the_posts_pagination();?>
         </div>
      </div>
        
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-12 pb-2">
          <h1><?php echo get_the_title(); ?></h1>
                            
          <?php      
                echo '<div class="row border-bottom pb-4 mb-4">';
                  echo '<div class="col-md-7">';
        
                          $image = get_field('location_image');
                          echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';
        
                          echo '<div class="border-bottom mb-2">'.get_field('location_description').'</div>';  

                          echo '<p><strong>Address:</strong><br/>';
                          echo ''.get_field('location_address_street').'<br/>';
                          echo ''.get_field('location_address_city').',&nbsp;';
                          echo ''.get_field('location_address_province').'&nbsp;';
                          echo ''.get_field('location_address_postal').'<br/>';
                          echo '<strong>Phone: </strong>'.get_field('location_phone_number').'';
        
              ?>
             
              <?php if( get_field('location_fax_number') ): ?>
                  &nbsp; | &nbsp;   <strong>Fax:</strong> <?php the_field('location_fax_number'); ?></a><br/><br/>
              <?php endif; ?>
       
               <?php    
                  echo '<p><strong>Store Hours:</strong></p>';
                  echo ''.get_field('location_hours').'';
 
                  echo '</div>';

                  echo '<div class="col-md-5" id="contact-form">';
          
                        echo '<div class="p-3 bg-light">';
                          echo ''.get_field('location_contact_form').'';
                        echo '</div>';
          
                      echo '</div>';
          
              echo '</div>';//row
          
              echo '<div class="row">';
           echo '<div class="embed-responsive embed-responsive-21by9"><iframe width="625" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ca/maps?ie=UTF8&amp;q='.get_field('location_address_street').','.get_field('location_address_province')[0].'+'.get_field('location_address_postal').'&amp;fb=1&amp;gl=ca&amp;output=embed"></iframe></div>';
          
          echo '</div>';
               ?>
      </div>
      
      <?php endwhile; endif; ?>
      
			<?php echo '<p><a href="/contact"><i class="fas fa-arrow-left"></i> View more Locations</a>'; ?>
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
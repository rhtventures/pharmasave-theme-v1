			<div class="row">
				
				<div class="col-md-12">
 					<div class="row ad-sm">
              <?php 

               $args = array(  
                        'post_type' => 'cpt_banner', 
                        'posts_per_page' => 100, 
                        'orderby'			=> 'menu_order',
                        'order'				=> 'ASC',
                        'tax_query' => array(
                          array(
                              'taxonomy' => 'banner_locations',
                              'field'    => 'slug',
                              'terms'    => 'sub-page-bottom'
                          ),
                         )
               );
            
              $loop = new WP_Query($args);
            
              while ( $loop->have_posts() ) : $loop->the_post();
            
                $image = get_field( 'banner_image' );  
                $link = get_field( 'banner_link' );
                
                echo '<div class="col-md-'.(get_field( 'banner_blocks' )*3).'">';
            
                    if( isset( $link['url'] ) )
                        echo '<a target="'.$link['target'].'" href="'.$link['url'].'" >';

                    echo '<img width="100%" src="'.$image['url'].'" class="img-fluid mb-3" alt="'.get_the_title().'" title="'.get_the_title().'"/>';

                    if( isset( $link['url'] ) )
                           echo '</a>';
            
                 echo  '</div>';
      
              endwhile;
              ?>

 					</div>
 				</div>
				
			</div>
<!--SUB MENU-->
 

<?php if( is_single() ): ?>

<?php 

    $_wp_query = $wp_query;

    //Replace with query for the page you want to masquerade as
    $wp_query=new WP_Query( array( 'post_type'=>get_post_type() ) );

    wp_nav_menu(
      array(
        'depth'             =>1,
        'sub_menu'			=> true,
        'container'         => '',
        'container_id'      => '',
        'container_class'   => '',
        'menu_class' 		=> 'list-unstyled sub-menu',
        'menu_id'			=> 'sub-menu',
      )
    );

    $wp_query = $_wp_query;

?>

<?php else: ?>

<?php 

  wp_nav_menu(
      array(
        'depth'             =>1,
        'sub_menu'			=> true,
        'container'         => '',
        'container_id'      => '',
        'container_class'   => '',
        'menu_class' 		=> 'list-unstyled sub-menu',
        //'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
        'menu_id'			=> 'sub-menu',
      )
  );  

?>  

<?php endif; ?>

<?php if( get_field('content_1') ): ?>
    <div class="side-content"><?php the_field('content_1'); ?></div>
<?php endif; ?>

<div class="row side-ads">

    <div id="subpage-banner" class="carousel slide carousel-fade" data-ride="carousel">
            <?php 
             $args = array(  
                      'post_type' => 'cpt_banner', 
                      'posts_per_page' => 100,
                      'orderby'	=> 'menu_order',
                      'order'	=> 'ASC',
                      'tax_query' => array(
                        array(
                            'taxonomy' => 'banner_locations',
                            'field'    => 'slug',
                            'terms'    => 'sub-page-right-side'
                        ),
                       )
             );

            $loop = new WP_Query($args); 

            ?>   



              <div class="carousel-inner">

                 <?php 
                    $slide_count = 0;
                    while ( $loop->have_posts() ) : $loop->the_post();

                        $image = get_field( 'banner_image' );  
                        $link = get_field( 'banner_link' );

                        echo '<div class="carousel-item '.(( $slide_count == 0 )? 'active' : '').'">';

                        if( isset( $link['url'] ) )
                            echo '<a target="'.$link['target'].'" href="'.$link['url'].'" >';

                        echo '<img src="'.$image['url'].'" class="img-fluid mb-3" alt="" title="'.get_the_title().'"/>';

                        if( isset( $link['url'] ) )
                               echo '</a>';

                        echo '</div>';

                    $slide_count++;
                    endwhile;
                ?>
  </div>

</div>
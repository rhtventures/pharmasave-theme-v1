<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-lg-9 pb-2">
          <h1><?php echo get_field('testimonial_fname').' '.get_field('testimonial_lname') ?></h1>
                            
          <ul>                    

            <?php              

              echo '<li>Title (post):'.get_the_title().'</li>';
              echo '<li>Archive Link: <a href="'.get_post_type_archive_link( 'cpt_testimonial' ).'">'.get_post_type_archive_link( 'cpt_testimonial' ).'</a></li>';                          
              echo '<li>First Name: '.get_field('testimonial_fname').'</li>';
              echo '<li>Last Name: '.get_field('testimonial_lname').'</li>';
              echo '<li>Title (role): '.get_field('testimonial_title').'</li>';   
              echo '<li>Quote: '.get_field('testimonial_quote').'</li>';

              $image = get_field('testimonial_image');
              echo '<li>Image: <img width="200" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></li>';

            ?>

          </ul>
         
      </div>
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>
<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5">
      
     
      <div class="col-lg-9 pb-2">
          <h1><?php post_type_archive_title(); ?></h1>

          <?php if(have_posts()) : while (have_posts()) : the_post(); ?>                  
                             

              <?php              
                echo '<div class="row border-bottom pb-4 mb-4">';
                  echo '<div class="col-sm-4">';
       
                     $image = get_field('blog_image');
                     echo '<a href="'.get_permalink().'"><img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/></a>';
        
                  echo '</div>';
                  
                  echo '<div class="col-sm-8">' ;  
                  echo '<span class="date">'.get_the_date('F j, Y').'</span>';
        
                      echo '<h3 class="mt-0">'.get_the_title().'</h3>';
                      //echo '<li>Summary: '.get_field('blog_summary').'</li>';
                       echo wp_trim_words(get_field('blog_article'), 60, '... <a href="'.get_permalink().'"> read more</a>' );
                  echo '</div>';
          
                echo '</div>';
                 
               ?>
            
           <?php endwhile;?>
        
              <?php else : ?>
                There are currently no news items.
              <?php endif; ?>
        
        <div class="col-12 py-2 mb-2">
             <?php echo ''.the_posts_pagination();?>
        </div>
        
      </div>

			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>


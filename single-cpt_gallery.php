<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?>  
      <div class="col-lg-9 pb-3 border-bottom">
        
          <h1><?php echo get_the_title(); ?></h1>
                            
          <?php              
            echo ''.get_field('gallery_summary').'<br/><br/>';

            $photos = get_field('gallery_photos');

            echo '<div class="row sm-gutter">';
                foreach( $photos as $photo ):
                    //print_r( $photo );
                    echo '<div class="col-6 col-md-4 mb-1">';

                        echo '<div class="image-crop">';
                            echo '<a href="'.$photo['url'].'" title="'.$photo['title'].'" data-toggle="lightbox" data-gallery="gallery" >'; 
                              
                                echo '<img class="ir" src="'.$photo['url'].'" alt="'.$photo['title'].'" title="'.$photo['title'].'"/>';
        
                            echo '</a>';
 
                         echo '</div>';
        
                         echo '<div class="pt-2 mb-4">'.$photo['title'].'</div>';

                    echo '</div>';
                 endforeach; 
        
             echo '</div>';
             
          ?>
         <?php echo '<div class="pt-4"> <a href="/galleries"><i class="fas fa-arrow-left"></i> View more Galleries </a> </div>'; ?>
      </div>
      
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section> 
<?php 	get_footer(); ?>
<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
</script>
<?php get_header(); ?>
 
<section>
	<div class="container pt-4">
		<div class="row pb-5 mb-5">
      
     <?php if(have_posts()) : while (have_posts()) : the_post(); ?> 
      
        <div class="col-lg-9 pb-2">
                             
          <?php      

              $date_start = str_replace('/', '-', get_field('event_date_start') );
              $date_end = str_replace('/', '-', get_field('event_date_end') );              
          
            echo '<div class="row mb-4">';
                  echo '<div class="col-sm-4">';

                      $image = get_field('event_image');
                      echo '<img class="ir mb-3" src="'.$image['url'].'" alt="'.$image['alt'].'" title="'.$image['title'].'"/>';

                  echo '</div>';

                  echo '<div class="col-sm-8">';
                      
                      echo '<h1 class="mt-0">'.get_the_title().'</h1>';

                        echo '<div class="date">';
        
                          if( $date_start != $date_end )
                              echo date( 'M d, Y', strtotime( $date_start ) ).'&nbsp;-&nbsp; '.date( 'M d, Y', strtotime( $date_end ) );
                          else
                              echo date( 'M d, Y', strtotime( $date_start ) );

                        echo '&nbsp;|&nbsp; '.get_field('event_location').'</div>';
          
          
                      echo ''.get_field('event_description').'';
                      
                      echo '<p><a href="/events"><i class="fas fa-arrow-left"></i> View more Events</a></p>';
          
                  echo '</div>';
              echo '</div>';//row
           
           ?>       
         
      </div>
       <?php endwhile; endif; ?>
      
			<div class="col-lg-3 right-column">
				<?php get_template_part('/page-templates-parts/right-column'); ?>
			</div><!--/right-column-->
 
		</div><!--/.row-->
    
    <section class="ads mb-5">
      
      <?php get_template_part('/page-templates-parts/ad-row'); ?>

		</section>
    
	</div><!--/.container-->
</section>

<?php 	get_footer(); ?>



          